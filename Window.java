// Swing import
import javax.swing.JPanel;
// import javax.swing.Timer;
// AWT imports
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class Window extends JPanel {
    // Variables:
    private WeekendKiller canvas;
    private int width;
    private int height;

    static final long serialVersionUID = 1L;

    // Constructors
    public Window() { this(500, 500); }

    public Window(int width, int height) {
        canvas = new WeekendKiller(width, height);
        this.width = width;
        this.height = height;
    }

    // Methods
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }
    
    @Override
    public void paintComponent(Graphics g) {
        // We clean the screen:
        super.paintComponent(g);
        // We draw over a copy:
        Graphics2D g2d = (Graphics2D) g.create();
        // A little test:
        // canvas.cDrawLine(10, 10, 50, 50, Color.BLACK);
        canvas.drawCube( 250, 250, 50, 20, Color.BLACK );
        g2d.drawImage(canvas.getImage(), 0, 0, width, height, this);
        // Dispose the copy to free some memory:
        g2d.dispose();
    }
}
