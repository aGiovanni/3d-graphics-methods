#! /bin/sh

if [ ! -d dist ]; then
  mkdir dist;
fi
javac *.java -d dist
cd dist
java Main
cd ..
