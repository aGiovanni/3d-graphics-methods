class Transform {

  public void Transform() {}

  public static int[] solveMatrix( int[][] matrix, int[] vector ) {
    int size = vector.length;
    int[] result = new int[ size ];
    for( int row = 0; row < size; row++ ) {
      result[ row ] = 0;
      for( int column = 0; column < size; column++ ) {
        result[ row ] += matrix[ row ][ column ] * vector[ column ];
      }
    }
    return result;
  }

  public static int[] translate( int x, int y, int z, int sx, int sy, int sz ) {
    int[][] matrix = {
      { 1, 0, 0, sx },
      { 0, 1, 0, sy },
      { 0, 0, 1, sz },
      { 0, 0, 0, 1 }
    };
    int[] vector = { x, y, z, 1 };
    return Transform.solveMatrix( matrix, vector );
  }

    // int[][] matrix = [
      // [],
      // [],
      // [],
      // [],
    // ];

}
