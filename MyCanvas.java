// AWT (Legacy) Imports
import java.awt.image.BufferedImage;
import java.awt.Color;

///////////////////////////////////////////////////////////////////////////////
//                                 ABSTRACT CLASS                            //
///////////////////////////////////////////////////////////////////////////////

public class MyCanvas {

    // Variables
    private BufferedImage buffer;

    private int width = 500, height = 500;

    // Constructors
    public MyCanvas() { this(500, 500); }
    public MyCanvas(int width, int height) {
        // se pidan Make the buffer image by dimension size (This is
        // where we draw the figures):
        buffer = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        this.width = width;
        this.height = height;
    }

    public BufferedImage getImage() { return this.buffer; }

    public void drawPixel( int x, int y, int color ) {
      if ( x > 0 && x < this.width && y > 0 && y < this.height ) {
        buffer.setRGB( x, y, color );
      }
    }

    public void drawPixel( int x, int y, Color color ) {
      this.drawPixel( x, y, color.getRGB() );
    }

    // Methods to draw in the buffer
    public void cDrawLine(int x0, int y0, int x1, int y1, Color color) {
        int rgbColor = color.getRGB();
        int dx = x1 - x0;
        int dy = y1 - y0;
        int stepX, stepY;
        int fraction;
        if (dx < 0) { dx = -dx; stepX = -1; } else { stepX = 1; }
        if (dy < 0) { dy = -dy; stepY = -1; } else { stepY = 1; }
        dx <<= 1;							// Same as "2 * dx"
        dy <<= 1;							// Same as "2 * dy"
        this.drawPixel( x0, y0, color.getRGB() );
        // Check if we draw in 'x' or 'y'
        if (dx > dy) {
            // Draw in 'x'
            fraction = dy - (dx >> 1);		// Same as "2 * dy - dx"
            while (x0 != x1) {
                if (fraction >= 0) {
                    y0 += stepY;
                    fraction -= dx;
                }
                x0 += stepX;
                fraction += dy;
                this.drawPixel( x0, y0, rgbColor );
            }
        } else {
            // Draw in 'y'
            fraction = dx - (dy >> 1);
            while (y0 != y1) {
                if (fraction >= 0) {
                    x0 += stepX;
                    fraction -= dy;
                }
                y0 += stepY;
                fraction += dx;
                this.drawPixel( x0, y0, rgbColor );
            }
        }
    }

    public void scanLine(int x, int y, int outline, int fill) {
        int i = x, j = y, auxY = y;
        while (buffer.getRGB(x, y) != outline) {
            while (buffer.getRGB(i, j) != outline) {
                buffer.setRGB(i, j, fill);
                i++;
            }
            i = x - 1;
            while (buffer.getRGB(i, j) != outline) {
                buffer.setRGB(i, j, fill);
                i--;
            }
            // Increase
            y++; j = y; i = x;
        }
        // Get back to the center position
        y = auxY - 1; j = y;
        while (buffer.getRGB(x, y) != outline) {
            while (buffer.getRGB(i, j) != outline) {
                buffer.setRGB(i, j, fill);
                i++;
            }
            i = x - 1;
            while (buffer.getRGB(i, j) != outline) {
                buffer.setRGB(i, j, fill);
                i--;
            }
            // Increase
            y--; j = y; i = x;
        }
    }

    public void cDrawRectangle(int x0, int y0, int x1, int y1, Color color) {
        // x1 = width	y = height
        // 1st horizontal line
        this.cDrawLine(x0, y0, x0 + x1, y0, color);
        // 2nd horizontal line
        this.cDrawLine(x0, y0 + y1, x0 + x1, y0 + y1, color);
        // 1st vertical line
        this.cDrawLine(x0, y0, x0, y0 + y1, color);
        // 2nd vertical line
        this.cDrawLine(x0 + x1, y0, x0 + x1, y0 + y1, color);
    }

    public void cFillRectangle(int x0, int y0, int x1, int y1,
            Color outline, Color fill) {
        // First, we draw the contour
        this.cDrawRectangle(x0, y0, x1, y1, outline);
        // Obtain the center coordinates
        int x = (x0 + x1 / 2);
        int y = (y0 + y1 / 2);
        // Now we fill the rectangle:
        this.scanLine(x, y, outline.getRGB(), fill.getRGB());
    }

    public void paintOctant(int x0, int y0, int x1, int y1, int rgbColor) {
        if (x1 == 0) {
            buffer.setRGB(x0, y0 + y1, rgbColor);
            buffer.setRGB(x0, y0 - y1, rgbColor);
            buffer.setRGB(x0 + y1, y0, rgbColor);
            buffer.setRGB(x0 - y1, y0, rgbColor);
        } else
            if (x1 <= y1) {
                buffer.setRGB(x0 + x1, y0 + y1, rgbColor);
                buffer.setRGB(x0 - x1, y0 + y1, rgbColor);
                buffer.setRGB(x0 + x1, y0 - y1, rgbColor);
                buffer.setRGB(x0 - x1, y0 - y1, rgbColor);
                if (x1 < y1) {
                    buffer.setRGB(x0 + y1, y0 + x1, rgbColor);
                    buffer.setRGB(x0 - y1, y0 + x1, rgbColor);
                    buffer.setRGB(x0 + y1, y0 - x1, rgbColor);
                    buffer.setRGB(x0 - y1, y0 - x1, rgbColor);
                }
            }
    }

    public void cDrawCircle(int x0, int y0, int radius, Color color) {
        int x = 0, y = radius, rgbColor = color.getRGB();
        // pk...
        double pk = 5/4 - radius;
        // Paint the fist octant:
        paintOctant(x0, y0, x, y, rgbColor);
        while (x < y) {
            if (pk < 0) {
                pk += 2 * x + 3;
                x++;
            } else {
                pk += 2 * (x - y) + 5;
                x++; y--;
            }
            paintOctant(x0, y0, x, y, rgbColor);
        }
    }

    public void cFillCircle(int x0, int y0, int radius, Color outline, Color fill) {
        // We draw the contour:
        cDrawCircle(x0, y0, radius, outline);
        // And we fill the circle:
        this.scanLine(x0, y0, outline.getRGB(), fill.getRGB());
    }
}
