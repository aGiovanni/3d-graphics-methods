// Swing imports
import javax.swing.JFrame;
// AWT imports
import java.awt.Color;
import java.awt.image.BufferedImage;

public class Screen extends JFrame {

    public Window panel;

    static final long serialVersionUID = 2L;

    public Screen() {
        super("3D Cubes!");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panel = new Window();
        setContentPane(panel);
        // Set the `JFrame` size to match the `panel` size
        pack();
        // setResizable(false);
        setVisible(true);
    }
}
