import java.awt.Color;
import java.util.Random;

public class WeekendKiller extends MyCanvas {

  /* Use this for parallel */
  // public int xc = 50, yc = 50, zc = -250;

  /* Use this for Perspective */
  public int xc = 25, yc = 25, zc = 50;
  public int unitSize = 5;

  public WeekendKiller() { super(); }
  public WeekendKiller( int width, int height ) { super( width, height ); }
  public WeekendKiller( int width, int height, int xc, int yc, int zc ) {
    this( width, height );
    this.setCenter( xc, yc, zc );
  }

  public Color getRandomColor() {
    Random rand = new Random();
    return new Color( rand.nextFloat(), rand.nextFloat(), rand.nextFloat() );
  }

  public void setCenter( int xc, int yc, int zc ) {
    this.xc = xc;
    this.yc = yc;
    this.zc = zc;
  }

  /* Draw line in perspective view */
  public void draw3Dline( int x1, int y1, int z1, int x2, int y2, int z2, Color color ) {
    this.zc = this.zc == 0 ? 1 : this.zc;

    int dz = z1 / this.zc;
    x1 = x1 - ( this.xc * dz );
    y1 = y1 - ( this.yc * dz );

    dz = z2 / this.zc;
    x2 = x2 - ( this.xc * dz );
    y2 = y2 - ( this.yc * dz );

    // this.cDrawLine( x1, y1, x2, y2, color );
    this.cDrawLine( x1, y1, x2, y2, this.getRandomColor() );
  }

  /* Draw line in parallel proyection */
  public void draw3Dline( int x1, int y1, int z1, int x2, int y2, int z2, Color color, boolean parallel ) {
    int dz = z1 - this.zc;
    x1 = this.xc - ( ( this.zc * ( x1 - this.xc ) ) / dz );
    y1 = this.yc - ( ( this.zc * ( y1 - this.yc ) ) / dz );

    dz = z2 - this.zc;
    x2 = this.xc - ( ( this.zc * ( x2 - this.xc ) ) / dz );
    y2 = this.yc - ( ( this.zc * ( y2 - this.yc ) ) / dz );

    this.cDrawLine( x1, y1, x2, y2, color );
  }

  public void drawCube( int x, int y, int z, int units, Color color ) {
    int d = units * this.unitSize;
    int[] translation = Transform.translate( x, y, z, -100, -100, 0 );
    x = translation[ 0 ];
    y = translation[ 1 ];
    z = translation[ 2 ];

    this.draw3Dline( x   , y   , z   , x +d, y   , z   , color );
    this.draw3Dline( x +d, y   , z   , x +d, y +d, z   , color );
    this.draw3Dline( x +d, y +d, z   , x   , y +d, z   , color );
    this.draw3Dline( x   , y +d, z   , x   , y   , z   , color );

    this.draw3Dline( x   , y   , z   , x   , y   , z +d, color );
    this.draw3Dline( x +d, y   , z   , x +d, y   , z +d, color );
    this.draw3Dline( x   , y +d, z   , x   , y +d, z +d, color );
    this.draw3Dline( x +d, y +d, z   , x +d, y +d, z +d, color );

    this.draw3Dline( x   , y   , z +d, x +d, y   , z +d, color );
    this.draw3Dline( x +d, y   , z +d, x +d, y +d, z +d, color );
    this.draw3Dline( x +d, y +d, z +d, x   , y +d, z +d, color );
    this.draw3Dline( x   , y +d, z +d, x   , y   , z +d, color );
  }

}
